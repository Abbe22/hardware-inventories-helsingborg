<?php

use Illuminate\Database\Seeder;

class CsvTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Task::class, 50)->create();
    }
}
