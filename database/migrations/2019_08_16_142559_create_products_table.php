<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {

           /* $table->string('name');
            $table->string('age');
            $table->string('job');
            $table->string('city');
            $table->string('hobby');*/
            $table->increments('id');
            $table->string('Stöldmärkning');
            $table->string('IMEI');
            $table->string('Kostnad');
            $table->string('Restvärde');
            $table->string('Hårdvarustatus');
            $table->string('Hårdvarutyp');
            $table->string('Kostnad hårdvara');
            $table->string('Kostnad Service');
            $table->string('Kostnad total');
            $table->string('Priskod hårdvara');
            $table->string('Priskod service');
            $table->string('Tillverkare');
            $table->string('Modell');
            $table->string('Namn');
            $table->string('Serienummer');
            $table->string('Organisation');
            $table->string('Lokation');
            $table->string('Kostnadställe');
            $table->string('Rum');
            $table->string('Tilldelat datum');
            $table->string('Mottaget datum');
            $table->string('Kontrakt');
            $table->string('Leverantör');
            $table->string('Ordernummer');
            $table->string('Primär användare');
            $table->string('Användarens kommentar');
            $table->string('Avskrivningsdatum');
            $table->string('Produktnummer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
