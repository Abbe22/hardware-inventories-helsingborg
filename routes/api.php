<?php
use App\Developer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Product;
use App\Http\Controllers\CSVController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('products', function() {
    return CSVController::parseCsv('file.csv');
});

Route::get('products/{id}', function($id){

    return Product::find($id);
} );