<?php
//use Illuminate\Routing\Route;    
use \Illuminate\Http\Request; 
use App\Product;
/* 
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|   
*/
  
Route::get('/:page', function () {
    return view('welcome');
});

//Import csv file to the database

Route::get('csv', 'CSVController@importCsv');
Route::post('csv', 'CSVController@importCsv');
//Route::post('csv', 'CSVController@preventDuplication');
//Route::get('csv', 'CSVController@preventDuplication');

//Route::get('csv', 'CSVController@viewCsv');

/*
//GET, POST, Update and Delete data in database. 
Route::get('show' ,'ItemController@items');
Route::get('add' ,'ItemController@additems');
Route::post('add' ,'ItemController@additems');
Route::get('add/{id}', function($id){
    $developer = Developer::find($id);
    $developer->delete();
    return redirect('show');
    });
Route::get('/edit/{id}' ,'ItemController@editItems');
Route::post('/edit/{id}' ,'ItemController@editItems');
*/

Auth::routes();









