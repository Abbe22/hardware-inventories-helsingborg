<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/csv.css">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Inventarieförteckning</title>
</head> 
<body >
    <div class="container">
        <div><font size="6"><b><img src="helsingborg_1445333232337.png" alt="Helsingborgslogo" 
        align="middle" width="100" height="130" font="40px"/>Inventarieförteckning</b></font></div>

    @if ($products) 
    <div class="table-responsive">
    <table class="table-sm table-striped">
        <!-- Return all the csv header as an array keys -->
        <thead>
        @if ($arrkeys = array_keys($products[0])) 
            @foreach ($arrkeys as $key)  
                <th>{{$key}}</th>
            @endforeach
        @endif 
        </thead>
            @foreach ($products as  $product)  
         
            <tr>
                <td> 
                {{$product['Stöldmärkning']}} 
                </td>
                <td>       
                {{$product['IMEI']}}
                </td>
                <td>
                {{$product['Kostnad']}}
                </td> 
                <td>
                {{$product['Restvärde']}}
                </td>
                <td>
                {{$product['Hårdvarustatus']}}
                </td>
                <td>       
                {{$product['Hårdvarutyp']}}
                </td>
                <td>       
                {{$product['Kostnad hårdvara']}}
                </td>
                <td>       
                {{$product['Kostnad Service']}}
                </td>
                <td>       
                {{$product['Kostnad total']}}
                </td>
                <td>       
                {{$product['Priskod hårdvara']}}
                </td>
                <td>       
                {{$product['Priskod service']}}
                </td>
                <td>       
                {{$product['Tillverkare']}}
                </td>
                <td>       
                {{$product['Modell']}}
                </td>
                <td>       
                {{$product['Namn']}}
                </td>
                <td>       
                {{$product['Serienummer']}}
                </td>
                <td>       
                {{$product['Organisation']}}
                </td>
                <td>       
                {{$product['Lokation']}}
                </td>
                <td>       
                {{$product['Kostnadställe']}}
                </td>
                <td>       
                {{$product['Rum']}}
                </td>
                <td>       
                {{$product['Tilldelat datum']}}
                </td>
                <td>       
                {{$product['Mottaget datum']}}
                </td>
                <td>       
                {{$product['Kontrakt']}}
                </td>
                <td>       
                {{$product['Leverantör']}}
                </td>
                <td>       
                {{$product['Ordernummer']}}
                </td>
                <td>       
                {{$product['Primär användare']}}
                </td>
                <td>       
                {{$product['Användarens kommentar']}}
                </td>
                <td>       
                {{$product['Avskrivningsdatum']}}
                </td>
                <td>       
                {{$product['Produktnummer']}}
                </td>
            </tr>
    
            @endforeach
    </table>
</div>     
    @endif
    <div>
</body>
</html>
<br>
