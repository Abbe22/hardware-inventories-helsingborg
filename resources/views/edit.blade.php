<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="UTF-8">  
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>    
<form action="/edit/{{$developer->id}}" method="POST">
     {{ csrf_field() }}
    Name: <input type="text" name="name" value="{{$developer->name}}" placeholder="Enter name"/>
    <br>
    Age: <input type="text" name="age" value="{{$developer->age}}" placeholder="Enter age"/>
    <br>
    Job: <input type="text" name="job" value="{{$developer->job}}" placeholder="Enter job"/> 
    <br>
    City: <input type="text" name="city" value="{{$developer->city}}" placeholder="Enter city"/>
    <br>
    Hobby: <input type="text" name="hobby" value="{{$developer->hobby}}" placeholder="Enter hobby"/>
    <br>
    <input type="submit" value="Edit employee"/>
</form>
</body>
</html>