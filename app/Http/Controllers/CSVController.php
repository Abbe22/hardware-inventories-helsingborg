<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Product;
use App\Http\Requests;
use Illuminate\Validation\Rule;
use DB;


class CSVController extends Controller 
{
    /**
     * Parse csv file to an array
     */
    static function parseCsv($csvFile) 
    {
        $products = []; 
        /**
         * Open and read csv file
         */
        if (($handle = fopen($csvFile, 'r')) !== false) {
            /**
             * Parse the csv file and returns an array 
             */
            if (($firstRow = fgetcsv($handle, 1000, ';' )) !== false)
            {

                while (($row = fgetcsv($handle, 1000, ';')) !== false)
                 {
                    if (count($firstRow) !== count($row)) {
                        return false;
                    } 
                    
                    $product = [];
                   /**
                    * Creates an array through one array for keys and another for its values
                    */
                    $product = array_combine($firstRow, $row);
                    /**
                     * Inserts the second array to the first one
                     */
                    array_push($products, $product); 
                }     
            }
            fclose($handle);
        }     
        return $products; 
    }
    /**
     * Import csv file into the database
     */
   public function importCsv()
    {    
        $products = $this::parseCsv('file.csv');
       
        if (!empty($products)) {
            /**
             * Save all csv products line by line with ability to avoid data duplication 
             * in the DB through function updateOrInsert.
             */
            foreach($products as $product) {
                /**
                 * Change id primary key to another one for possibility changing in the original file.
                 */
                $productId = $product['Stöldmärkning'];
                unset($product['id']);

                DB::table('products')->updateOrInsert(
                [ 'Stöldmärkning' =>  $productId],
                    $product
                );      
            }
        }
        // Presenting the file
        return view('csv')->with('products', $products);
    }  
}


