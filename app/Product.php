<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    // 
  
    /*protected $fillable = ["id","name","age","job","city","hobby"];*/
    protected $fillable = ["Stöldmärkning","IMEI","Kostnad","Restvärde","Hårdvarustatus",
    "Hårdvarutyp","Kostnad hårdvara","Kostnad Service","Kostnad total","Priskod hårdvara",
    "Priskod service","Tillverkare","Modell","Namn","Serienummer","Organisation","Lokation",
    "Kostnadställe","Rum","Tilldelat datum","Mottaget datum","Kontrakt","Leverantör",
    "Ordernummer","Primär användare","Användarens kommentar","Avskrivningsdatum","Produktnummer"
   ];

   public $timestamps = false;
   protected $primaryKey = 'Stöldmärkning';

}
 